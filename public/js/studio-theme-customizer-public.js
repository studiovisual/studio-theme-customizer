(function( $ ) {
	'use strict';

	/**
	 * All of the code for your public-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */

	$(window).load(function(){
	    //necessario para dar override no padrao do tema
	    $('.main-menu.menu .sub-menu > li > a').each(function(){
	       $(this).addClass('link');
        });

		$('#menu-item-3280 li').each(function(){
			$(this).addClass('sub-menu-item pink');
		});

		$('#menu-item-3281 li').each(function(){
			$(this).addClass('sub-menu-item orange');
		});

		$('#menu-item-3282 li').each(function(){
			$(this).addClass('sub-menu-item blue');
		});

		$('#menu-item-92 li').each(function(){
			$(this).addClass('sub-menu-item green');
		});
	});
})( jQuery );
