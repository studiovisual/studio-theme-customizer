<?php

/**
 * Fired during plugin activation
 *
 * @link       a
 * @since      1.0.0
 *
 * @package    Studio_Theme_Customizer
 * @subpackage Studio_Theme_Customizer/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Studio_Theme_Customizer
 * @subpackage Studio_Theme_Customizer/includes
 * @author     Studio Visual <loliva@studiovisual.com.br>
 */
class Studio_Theme_Customizer_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
