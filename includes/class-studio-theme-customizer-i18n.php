<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       a
 * @since      1.0.0
 *
 * @package    Studio_Theme_Customizer
 * @subpackage Studio_Theme_Customizer/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Studio_Theme_Customizer
 * @subpackage Studio_Theme_Customizer/includes
 * @author     Studio Visual <loliva@studiovisual.com.br>
 */
class Studio_Theme_Customizer_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'studio-theme-customizer',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
