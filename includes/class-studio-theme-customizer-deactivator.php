<?php

/**
 * Fired during plugin deactivation
 *
 * @link       a
 * @since      1.0.0
 *
 * @package    Studio_Theme_Customizer
 * @subpackage Studio_Theme_Customizer/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Studio_Theme_Customizer
 * @subpackage Studio_Theme_Customizer/includes
 * @author     Studio Visual <loliva@studiovisual.com.br>
 */
class Studio_Theme_Customizer_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
