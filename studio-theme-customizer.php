<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              a
 * @since             1.0.0
 * @package           Studio_Theme_Customizer
 *
 * @wordpress-plugin
 * Plugin Name:       studio-theme-customizer
 * Plugin URI:        https://www.studiovisual.com.br/
 * Description:       Plugin utilizado para dar override no CSS e JS do tema ativo.
 * Version:           1.0.0
 * Author:            Studio Visual
 * Author URI:        a
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       studio-theme-customizer
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'STUDIO_THEME_CUSTOMIZER_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-studio-theme-customizer-activator.php
 */
function activate_studio_theme_customizer() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-studio-theme-customizer-activator.php';
	Studio_Theme_Customizer_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-studio-theme-customizer-deactivator.php
 */
function deactivate_studio_theme_customizer() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-studio-theme-customizer-deactivator.php';
	Studio_Theme_Customizer_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_studio_theme_customizer' );
register_deactivation_hook( __FILE__, 'deactivate_studio_theme_customizer' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-studio-theme-customizer.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_studio_theme_customizer() {

	$plugin = new Studio_Theme_Customizer();
	$plugin->run();

}
run_studio_theme_customizer();
