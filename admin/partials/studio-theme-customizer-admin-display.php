<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       a
 * @since      1.0.0
 *
 * @package    Studio_Theme_Customizer
 * @subpackage Studio_Theme_Customizer/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
